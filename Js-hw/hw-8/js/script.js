const createNewBlok = () => {
    const createBtn = document.getElementById('createBtn');

    const item = document.createElement('div');
    item.innerHTML = 'NEW BOLK';
    document.body.appendChild(item);
}

function delItems() {
    const body = document.body;
    const divs = body.getElementsByTagName('div');
    
    if (divs.length > 10) {
        let i = 0;
        while (i < divs.length) {
            body.removeChild(divs[i]);
        }
    }
}

document.addEventListener('DOMSubtreeModified', delItems);
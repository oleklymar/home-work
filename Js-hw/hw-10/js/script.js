class Hamburger {
    constructor (size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;

        this.price = 0;
        this.calories = 0;

        if (size !== 'small' && size !== 'large') {
            throw new Error('Не правильний розмір бургера!!!');
        }

        if (stuffing !== 'cheese' && stuffing !== 'salad' && stuffing !== 'potato') {
            throw new Error('Не має такої начинки!!!');
        }
    }

    // Метод для добавок

    addTopping (topping) {
        if (topping !== 'spice' && topping !== 'mayo'){
            throw new Error ('Неіснує такої добавки');
        }

        // Ціна та калорії на добавки

        if (topping === 'spice') {
            this.price += 15;
        }
        else {
            this.price += 20;
            this.calories += 5;
        }
        
    }

    // Метод для візначення ціни

    calculatePrice () {

        // Визначаємо ціну в залежності від розміру

        if (this.size === 'small') {
            this.price += 50;
        }
        else {
            this.price += 100;
        }

        // Визначаємо ціну начинки

        if (this.stuffing === 'cheese') {
            this.price += 10;
        }
        else if (this.stuffing === 'salad') {
            this.price += 20;
        }
        else {
            this.price += 15;
        }

        return this.price;
    }

    // Метод на визначення калорій

    calculateCalories () {

        // Базова калорійність

        if (this.size === 'small') {
            this.calories += 20;
        }
        else {
            this.calories += 40;
        }

        if (this.stuffing === 'cheese') {
            this.calories += 20;
        }
        else if (this.stuffing === 'salad') {
            this.calories += 5;
        }
        else {
            this.calories += 10;
        }

        return this.calories;
    }
}

try {
    var hamburger = new Hamburger ('small', 'cheese');
    hamburger.addTopping('mayo');
    hamburger.addTopping('spice');

    var totalPrice = hamburger.calculatePrice();
    var totalCalories = hamburger.calculateCalories();

    console.log('Ціна: ' + totalPrice + ' гривень');
    console.log('Калорії: ' + totalCalories + ' Калорій');
}
catch (error ) {
    console.log(error.message);
}
const boxImg = document.querySelector('.box-img');
const imgList = document.querySelectorAll('img');
const prevBtn = document.querySelector('.prev-btn');
const nextBtn = document.querySelector('.next-btn');
const countImg = imgList.length;
let nextImg = 0;


prevBtn.addEventListener('click', () => {
  nextImg = (nextImg - 1 + countImg) % countImg;
  slide();
});

nextBtn.addEventListener('click', () => {
  nextImg = (nextImg + 1) % countImg;
  slide();
});

const slide = () => {
  const imageWidth = boxImg.clientWidth;
  const slideOffset = -nextImg * imageWidth;
  boxImg.style.transform = `translateX(${slideOffset}px)`;
}

window.addEventListener('load', () => {
  slide();
});




/*const imgList = document.querySelectorAll('img');
const childList = document.querySelector('#imgs');
let node = null;

const selectNextNode = () => {
  resetVisible();
  if (node == null) {
      
      node = childList.firstChild;
      node.setAttribute('style', 'display:flex');
      return;
  }
  // Получение следующего элемента, которые в дереве находиться на одном уровне.
  node = node.nextSibling;
  if (node != null) {
      node.setAttribute('style', 'display:flex');
  }
}

const selectPrevNode = () => {
  resetVisible();
  if (node == null) {

      node = childList.lastChild;
      node.setAttribute('style', 'display:flex');
      return;
  }
  // Получение предыдущего элемента, которые в дереве находиться на одном уровне.
  node = node.previousSibling;
  if (node != null) {
      node.setAttribute('style', 'display:flex');
  }
}

const resetVisible = () => {
  imgList.forEach(element => {
    element.setAttribute('style', 'display:none');
  });
}

window.onload = () => {
  const imgList = document.querySelectorAll('.hiden');

  imgList.forEach(element => {
    element.setAttribute('style', 'display:none');
  });
}*/

/*let node = null;

const selectNextNode = () => {
  resetVisible();
  if (node == null) {
      const imgs = document.querySelector('#imgs');
      
      node = imgs.firstChild;
      node.setAttribute('style', 'visibility:visible');
      return;
  }
  // Получение следующего элемента, которые в дереве находиться на одном уровне.
  node = node.nextSibling;
  if (node != null) {
      node.setAttribute('style', 'visibility:visible');
  }
}

const selectPrevNode = () => {
    resetVisible();
    if (node == null) {
        const imgs = document.querySelector('#imgs');
        node = imgs.lastChild;
        node.setAttribute('style', 'visibility:visible');
        return;
    }
    // Получение предыдущего элемента, которые в дереве находиться на одном уровне.
    node = node.previousSibling;
    if (node != null) {
        node.setAttribute('style', 'visibility:visible');
    }
}

const resetVisible = () => {
  const imgList = document.querySelectorAll('img');

  imgList.forEach(element => {
    element.setAttribute('style', 'visibility:hidden');
  });
}*/

/*window.onload = () => {
  const imgList = document.querySelectorAll('.hiden');

  imgList.forEach(element => {
    element.setAttribute('style', 'visibility:hidden');
  });
}*/
const circles = document.querySelector('.circles');

const createCircles = () => {
    let parameters = prompt('Set the diameter of the circles (no more than 100 pixels', 85);
    buildCircles(parameters);
}

const buildCircles = (parameters) => {
    let template = '';
    for (let i = 0; i < 100; i++) {
        let circleColor = `rgb(${String(Math.floor(Math.random() * 360))}, ${String(Math.floor(Math.random() * 360))},${String(Math.floor(Math.random() * 360))})`;
        let circle = `<div class="circle" style="width: ${parameters}px; height: ${parameters}px; background-color: ${circleColor};"></div>`;
        template += circle;
    }
    circles.innerHTML = template;
}
circles.addEventListener('click', ({target}) => {
    if(target.matches('.circle')) {
        target.style.display = 'none';
    }
})
